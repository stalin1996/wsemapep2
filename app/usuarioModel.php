<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class usuarioModel extends Model implements Authenticatable
{
	use AuthenticableTrait;
    protected $table='usuario';
    protected $guarded=['id'];
    protected $hidden = [
   		'password'
   ];
}
