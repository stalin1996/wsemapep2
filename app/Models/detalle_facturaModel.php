<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detalle_facturaModel extends Model
{
   protected $table='detallefactura';
   protected $primaryKey = 'serial_def';
   public $timestamps = false;


    public function cabecera()
    {
        return $this->belongsTo('App\Models\cabecera_facturaModel','serial_caf');
    }
}
