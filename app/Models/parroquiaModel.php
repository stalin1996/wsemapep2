<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class parroquiaModel extends Model
{
   protected $table='parroquia';
   protected $primaryKey = 'serial_prq';
   public $timestamps = false;


   public function canton()
    {
        return $this->belongsTo('App\Models\cantonModel','serial_ctn');
    }
}
