<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cabecera_tarifaModel extends Model
{
   protected $table='cabeceratarifas';
   protected $primaryKey = 'serial_ctar';
   public $timestamps = false;

}
