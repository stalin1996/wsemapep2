<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class valoresModel extends Model
{
   protected $table='tablavalores';
   protected $primaryKey = 'serial_tbv';
   public $timestamps = false;
}
