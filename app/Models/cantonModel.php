<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class parroquiaModel extends Model
{
   protected $table='canton';
   protected $primaryKey = 'serial_ctn';
   public $timestamps = false;
}
