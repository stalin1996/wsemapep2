<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class catastroModel extends Model
{
   protected $table='catastros';
   protected $primaryKey = 'serial_cat';
   public $timestamps = false;


   public function parroquia()
    {
        return $this->belongsTo('App\Models\parroquiaModel','serial_prq');
    }
}
