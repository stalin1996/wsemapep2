<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\usuarioModel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Validator;
class usuarioController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    public function index(){
        return response()->json(usuarioModel::all());
    }

    public function Store(Request $request){

          $validator = Validator::make($request->all(), [
           'email' => 'required|email|unique:usuario',
           'identificacion' => 'required|unique:usuario'
        ],[
          'email.unique'=>'Correo electrónico ya fue registrado',
          'identificacion.unique'=>'Identificación ya fue registrado'
        ]);

        if ($validator->fails()) {
          $messages = $validator->errors();
           return response()->json($messages, 500);
        }

        $nuevo_usuario= new usuarioModel();
        $nuevo_usuario->rol_id=1;
        $nuevo_usuario->nombres=$request->nombres;
        $nuevo_usuario->apellidos=$request->apellidos;
        $nuevo_usuario->identificacion=$request->identificacion;
        $nuevo_usuario->email=$request->email;
        $nuevo_usuario->password=Hash::make($request->password);
        if($nuevo_usuario->save()){
            return response()->json($nuevo_usuario);
        }else{
             return response()->json("error");
        }
    }

     public function Update(Request $request,$id){
        $validator = Validator::make($request->all(), [
           'email' => 'required|email|unique:usuario',
           'identificacion' => 'required|unique:usuario'
        ],[
          'email.unique'=>'Correo electrónico ya fue registrado',
          'identificacion.unique'=>'Identificación ya fue registrado'
        ]);
        $nuevo_usuario= usuarioModel::find($id);
        $nuevo_usuario->rol_id=1;
        $nuevo_usuario->nombres=$request->nombres;
        $nuevo_usuario->apellidos=$request->apellidos;
        $nuevo_usuario->identificacion=$request->identificacion;
        $nuevo_usuario->email=$request->email;
        $nuevo_usuario->password=bcrypt($request->password);
        if($nuevo_usuario->save()){
            return response()->json($nuevo_usuario);
        }else{
             return response()->json("error");
        }
    }

    public function Destroy($id){
        usuarioModel::find($id)->delete();
        return 1;
    }

/*

    public function Authenticate(Request $request)
   {
       $this->validate($request, [
       'email' => 'required',
       'password' => 'required'
        ]);
      $user = usuarioModel::where('email', $request->input('email'))->first();
     if(Hash::check($request->input('password'), $user->password)){
          $token = base64_encode(str_random(40));
          usuarioModel::where('email', $request->input('email'))->update(['token' => "$token"]);;
          return response()->json(['status' => 'success','token' => $token]);
      }else{
          return response()->json(['status' => 'fail'],401);
      }
   }   
   */
}