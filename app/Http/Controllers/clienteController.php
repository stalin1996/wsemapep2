<?php

namespace App\Http\Controllers;
use App\Models\clienteModel;
use App\Models\catastroModel;
use App\Models\instalacionModel;
use App\Models\cabecera_facturaModel;
use App\Models\tarifaModel;
use App\Models\detalle_facturaModel;
use App\Models\valoresModel;
use Illuminate\Support\Facades\DB;
class clienteController extends Controller
{
    function obtener_consumo_cliente($tipo,$valor) {

        $where_colum='0=1';

        if($tipo==1){
            $where_colum="cl.ciruc_cli='$valor'";
        }

        if($tipo==2){
            $where_colum="i.numerocuenta_ins='$valor'";
        }

        if($tipo==3){
            $where_colum="c.claveCatastral_cat='$valor'";
        }

        if($tipo==4){
            $where_colum="cf.periodo_caf like '$valor%'";
        }
    	ini_set('max_execution_time', 3000);
        $consumo = DB::Table('cabecerafactura as cf')
        ->leftjoin('detallefactura as df',function($join) {
            $join->on('df.serial_caf','=','cf.serial_caf')
            ->where('df.serial_ite',1);
        })->leftjoin('instalacion as i','i.serial_ins','cf.serial_ins')
        ->leftjoin('catastros as c','c.serial_cat','i.serial_cat')
        ->leftjoin('parroquia as p','p.serial_prq','c.serial_prq')
        ->leftjoin('cliente as cl','cl.serial_cli','i.serial_cli')
        ->where(function ($query){
            $query->whereNull('cf.motivoanulacion_caf')
            ->Orwhere('cf.motivoanulacion_caf','F');
        })->whereraw($where_colum)
        ->where(function ($query)use($tipo){
            if($tipo==4){
                $query->where('p.serial_ctn',3)
                ->Orwhere('c.claveCatastral_cat','like','5%');
            }
        })->OrderBy('cf.periodo_caf','DESC')
        ->select('cf.serial_caf',
            'cf.periodo_caf as periodo',
            'cf.consumo_caf as consumo',
            'cl.ciruc_cli as identificacion_cliente',
            DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli) as apellidos_cliente"),
            DB::raw("CONCAT(nombre1_cli,' ',nombre2_cli) as nombres_cliente"),
            'i.numerocuenta_ins as cuenta',
            'c.direccion_cat as direccion',
            'p.descripcion_prq',
            'c.claveCatastral_cat as clave_catastral',
            DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli,' ',nombre1_cli,' ',nombre2_cli) as cliente"),
            'i.serial_clc as clase_cliente',
            'cf.serial_tar',
            'i.serial_tca',
            'motivoanulacion_caf',
            'serial_ite',
            'p.serial_prq as parroquia_id',
            'p.descripcion_prq as parroquia_nombre'
        )->get();
        foreach ($consumo as $con) {
            $consumos=$this->consumo_agua($con->serial_caf,$con->clase_cliente,$con->consumo,$con->serial_tca);
            $con->valor_sin_descuento=$consumos['inicial'];
            $con->valor_agua=$consumos['valor_agua'];
            $con->valor_alcantarillado=$consumos['valor_alcantarillado'];
        }

        return response()->json($consumo);
    }


    function consumo_agua($serial_caf,$clase_cliente,$consumo,$tca){
            $valor=0;
            $valor_alcantarillado=2;
            $tarifa_alcantarillado=2;

            if($tca==3){
                $valor_alcantarillado=18;
                $tarifa_alcantarillado=18;
            }

            if($tca==2){
                $valor_alcantarillado=5;
                $tarifa_alcantarillado=5;
            }




            $inicial=0;
            $det=detalle_facturaModel::where('serial_caf',$serial_caf)
            ->where('serial_ite',1)
            ->first();
            if(!is_null($det)){
                $valor=$det->valorapagar_def;
                $inicial=$valor;
                    $valor_alcantarillado=$valor/2;
                    if($clase_cliente!=1){
                        if($consumo>0){
                            $datos_consumos_base=array(2 =>  'm3TerceraEdad',3 => 'm3Discapacitados');
                            $datos_porce=array(2 => 'PorceDesctoTE',3 => 'PorceDesctoDiscap');
                            $c_base=valoresModel::where('codigo_tbv',$datos_consumos_base[$clase_cliente])->first()->valor_tbv;
                            $c_base=floatval($c_base);                                    
                            $porc=valoresModel::where('codigo_tbv',$datos_porce[$clase_cliente])->first()->valor_tbv;
                            $porc=floatval($porc);

                                    $valor=($valor*$porc)/100;
                                    if($consumo>$c_base){
                                        $valor_metro=round(floatval($det->valorapagar_def/$consumo),2);
                                        //valor de los 34 metros
                                        $con1=$c_base*$valor_metro;
                                        $con2=$consumo-$c_base;
                                        $con1=($con1*$porc)/100;
                                        $con2=($con2*$valor_metro);
                                        $valor=$con1+$con2;
                                    }
                                    $valor_alcantarillado=$valor/2;
                                    if($valor_alcantarillado<2){
                                        $valor_alcantarillado=2;
                                    }


                            }
                        }   

                        if($valor_alcantarillado<$tarifa_alcantarillado){
                            $valor_alcantarillado=$tarifa_alcantarillado;
                        }                 
            }
             $valor=round($valor,2);
             $valor_alcantarillado=round($valor_alcantarillado,2);
            return array('valor_agua'=>$valor,'valor_alcantarillado'=>$valor_alcantarillado,'inicial'=>$inicial);

            //ctar 2 o 4 ->zona 2
    }


    function obtener_cliente($tipo,$valor){
        
        ini_set('max_execution_time', 3000);

        $valor = str_replace('%20', ' ', $valor); 
        $valor = str_replace('%C3%91', 'Ñ', $valor);
        $valor=mb_strtoupper($valor);


        $where_dato='0=1';   

        if($tipo=="codigo"){
            $where_dato="instalacion.numerocuenta_ins like '%$valor%'";
        }

         if($tipo=="direccion"){
            $where_dato="catastros.direccion_cat='$valor'";
        } 

        if($tipo=="nombres"){
            $where_dato="CONCAT(apellido1_cli,' ',apellido2_cli,' ',nombre1_cli,' ',nombre2_cli) like '%$valor%'";
        }


        $consumo=clienteModel::join('instalacion','instalacion.serial_cli','cliente.serial_cli')
        ->join('catastros','catastros.serial_cat','instalacion.serial_cat')  
        ->whereraw($where_dato)  
        ->OrderBy('instalacion.numerocuenta_ins')
        ->select('instalacion.numerocuenta_ins as codigo','catastros.direccion_cat as direccion',DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli,' ',nombre1_cli,' ',nombre2_cli) as nombres"))
        ->get();
        return response()->json($consumo);
    }

    //sucre seria ctn 3 codigo ctn 14
    /*
    si tiene regulacion entonces anula a facutra y genera una nueva factura



    ver si tiene regulacion y tomar la nueva
    si esta anulada y en tiocalculo esta en estado e regulacion r o s, 


    */
    public function obtener_consumo_agua_o_un_detalle($coleccion)
    {
        $almacen = collect();
        foreach ($coleccion as $key => $colec) {
            $verificar = $almacen->where('cuenta',$colec->cuenta)->where('periodo',$colec->periodo);
            if (!count($verificar)) {
                $consulta = $coleccion->where('cuenta',$colec->cuenta)->where('periodo',$colec->periodo);
                if ($consulta->contains('serial_ite', 1)) {
                    $sacarDetalle = $consulta->where('serial_ite', 1)->first();
                }else {
                    $sacarDetalle = $consulta->first();
                }
                $almacen->push($sacarDetalle);
            }
        }
        return $almacen;
    }

    public function obtenerdeudasclientes2($tipo,$valor)
    {
        $where_colum='0=1';
        if($tipo==1){
            $where_colum="cliente.ciruc_cli='$valor'";
        }

        if($tipo==2){
            $where_colum="instalacion.numerocuenta_ins='$valor'";
        }

        if($tipo==3){
            $where_colum="catastros.claveCatastral_cat='$valor'";
        }

        if($tipo==4){
            $where_colum="cabecerafactura.periodo_caf like '$valor%'";
        }

        ini_set('max_execution_time', 3000);
        $consumo = DB::Table('cabecerafactura as cf')
        ->leftjoin('detallefactura as df',function($join) {
            $join->on('df.serial_caf','=','cf.serial_caf')
            ->where('df.serial_ite',1);
        })->leftjoin('instalacion as i','i.serial_ins','cf.serial_ins')
        ->leftjoin('catastros as c','c.serial_cat','i.serial_cat')
        ->Leftjoin('parroquia as p','p.serial_prq','c.serial_prq')
        ->Leftjoin('cliente as cl','cl.serial_cli','i.serial_cli')
        ->where(function ($query){
            $query->whereNull('cf.motivoanulacion_caf')
            ->Orwhere('cf.motivoanulacion_caf','F');
        })->whereraw($where_colum)
        ->where(function ($query)use($tipo){
            if($tipo==4){
                $query->where('p.serial_ctn',3)
                ->Orwhere('c.claveCatastral_cat','like','5%');
            }
        })->OrderBy('cf.periodo_caf','DESC')
        ->select('cf.serial_caf',
            'cf.periodo_caf as periodo',
            'cf.consumo_caf as consumo',
            'cl.ciruc_cli as identificacion_cliente',
            DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli) as apellidos_cliente"),
            DB::raw("CONCAT(nombre1_cli,' ',nombre2_cli) as nombres_cliente"),
            'i.numerocuenta_ins as cuenta',
            'c.direccion_cat as direccion',
            'p.descripcion_prq',
            'c.claveCatastral_cat as clave_catastral',
            DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli,' ',nombre1_cli,' ',nombre2_cli) as cliente"),
            'i.serial_clc as clase_cliente',
            'cf.serial_tar',
            'i.serial_tca',
            'motivoanulacion_caf',
            'serial_ite'
        )->get();
        dd($consumo);
        $consumo = cabecera_facturaModel::join('instalacion','instalacion.serial_ins','cabecerafactura.serial_ins')
        ->join('catastros','catastros.serial_cat','instalacion.serial_cat')
        ->Leftjoin('parroquia','parroquia.serial_prq','catastros.serial_prq')
        ->join('cliente','cliente.serial_cli','instalacion.serial_cli')
        ->whereraw($where_colum)
        ->where(function ($query)use($tipo){
            if($tipo==4){
            $query->where('parroquia.serial_ctn',3)
            ->Orwhere('catastros.claveCatastral_cat','like','5%');
            }
        })->where(function ($query){
            $query->whereNull('cabecerafactura.motivoanulacion_caf')
            ->Orwhere('cabecerafactura.motivoanulacion_caf','F');
        })->OrderBy('periodo_caf','DESC')
        ->select('cabecerafactura.periodo_caf as periodo','cliente.ciruc_cli as identificacion_cliente',DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli) as apellidos_cliente"),DB::raw("CONCAT(nombre1_cli,' ',nombre2_cli) as nombres_cliente"),'instalacion.numerocuenta_ins as cuenta','catastros.direccion_cat as direccion','parroquia.descripcion_prq','catastros.claveCatastral_cat as clave_catastral',DB::raw("CONCAT(apellido1_cli,' ',apellido2_cli,' ',nombre1_cli,' ',nombre2_cli) as cliente"),'instalacion.serial_clc as clase_cliente','instalacion.serial_tca')
        ->groupBy('cabecerafactura.periodo_caf','cliente.ciruc_cli',"apellido1_cli",'apellido2_cli','nombre1_cli','nombre2_cli',"nombres_cliente",'instalacion.numerocuenta_ins','catastros.direccion_cat','parroquia.descripcion_prq','catastros.claveCatastral_cat','instalacion.serial_clc','instalacion.serial_tca')
        ->first();
        dd($consumo);
        foreach ($consumo as $con) {
            $consumos=$this->consumo_agua($con->serial_caf,$con->clase_cliente,$con->consumo,$con->serial_tca);
            $con->valor_sin_descuento=$consumos['inicial'];
            $con->valor_agua=$consumos['valor_agua'];
            $con->valor_alcantarillado=$consumos['valor_alcantarillado'];
        }
    }

    public function obtenerdeudasneo($value='')
    {
            $valor=0;
            $valor_alcantarillado=2;
            $tarifa_alcantarillado=2;

            if($tca==3){
                $valor_alcantarillado=18;
                $tarifa_alcantarillado=18;
            }

            if($tca==2){
                $valor_alcantarillado=5;
                $tarifa_alcantarillado=5;
            }




            $inicial=0;
            $det=detalle_facturaModel::where('serial_caf',$serial_caf)
            ->where('serial_ite',1)
            ->first();
            if(!is_null($det)){
                $valor=$det->valorapagar_def;
                $inicial=$valor;
                    $valor_alcantarillado=$valor/2;
                    if($clase_cliente!=1){
                        if($consumo>0){
                            $datos_consumos_base=array(2 =>  'm3TerceraEdad',3 => 'm3Discapacitados');
                            $datos_porce=array(2 => 'PorceDesctoTE',3 => 'PorceDesctoDiscap');
                            $c_base=valoresModel::where('codigo_tbv',$datos_consumos_base[$clase_cliente])->first()->valor_tbv;
                            $c_base=floatval($c_base);                                    
                            $porc=valoresModel::where('codigo_tbv',$datos_porce[$clase_cliente])->first()->valor_tbv;
                            $porc=floatval($porc);

                                    $valor=($valor*$porc)/100;
                                    if($consumo>$c_base){
                                        $valor_metro=round(floatval($det->valorapagar_def/$consumo),2);
                                        //valor de los 34 metros
                                        $con1=$c_base*$valor_metro;
                                        $con2=$consumo-$c_base;
                                        $con1=($con1*$porc)/100;
                                        $con2=($con2*$valor_metro);
                                        $valor=$con1+$con2;
                                    }
                                    $valor_alcantarillado=$valor/2;
                                    if($valor_alcantarillado<2){
                                        $valor_alcantarillado=2;
                                    }


                            }
                        }   

                        if($valor_alcantarillado<$tarifa_alcantarillado){
                            $valor_alcantarillado=$tarifa_alcantarillado;
                        }                 
            }
             $valor=round($valor,2);
             $valor_alcantarillado=round($valor_alcantarillado,2);
            return array('valor_agua'=>$valor,'valor_alcantarillado'=>$valor_alcantarillado,'inicial'=>$inicial);

            //ctar 2 o 4 ->zona 2
    }
}